from setuptools import setup

package_name = 'voltron_dashboard'

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='sahan',
    maintainer_email='sreddy13@students.kennesaw.edu',
    description='TODO: Package description',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'voltron_dashboard_node = voltron_dashboard.voltron_dashboard_node:main'
        ],
    },
)
